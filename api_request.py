import requests
from tools.image_utils import *
from config import *
from tqdm import tqdm
import json 
import os 

def stable_diffusion_generater_img2img(prompt: str, 
                                negative_prompt: str,
                                init_images: list,
                                styles: list = [], 
                                cfg_scale: float = 30.0,
                                sampler_index: str = "DDIM",
                                batch_size: int = 4, 
                                steps: int = 50, 
                                seed: int = None):
    
    # REQUEST BODY
    request_body_img2img["prompt"] = prompt
    request_body_img2img["negative_prompt"] = negative_prompt
    request_body_img2img["init_images"] = init_images
    request_body_img2img["styles"] = styles
    request_body_img2img["cfg_scale"] = cfg_scale
    request_body_img2img["sampler_index"] = sampler_index
    request_body_img2img["batch_size"] = batch_size
    request_body_img2img["steps"] = steps
    request_body_img2img["seed"] = seed

    res = requests.post(URL, json=request_body_img2img)
    res = res.json()
    images = []
    for image_base64 in res["images"]:
        image = base64_to_image(image_base64)
        images.append(image)

    return images

def stable_diffusion_generater_txt2img( prompt: str, 
                                negative_prompt: str,
                                styles: list = [], 
                                cfg_scale: float = 7.5,
                                sampler_index: str = "DDIM",
                                batch_size: int = 4, 
                                steps: int = 50, 
                                seed: int = None):
    
    # REQUEST BODY
    request_body["prompt"] = prompt
    request_body["negative_prompt"] = negative_prompt
    request_body["styles"] = styles
    request_body["cfg_scale"] = cfg_scale
    request_body["sampler_index"] = sampler_index
    request_body["batch_size"] = batch_size
    request_body["steps"] = steps
    request_body["seed"] = seed

    res = requests.post(URL, json=request_body)
    res = res.json()
    images = []
    for image_base64 in res["images"]:
        image = base64_to_image(image_base64)
        images.append(image)

    return images


def main():
    init_img = Image.open("/home/namnh/CS410/API/city_and_scene/img01.jpg").convert("RGB")
    init_img = np.array(init_img)
    init_img_base64 = image_to_base64(init_img)

    prompt = "City life in 2050, cubic, surreal art by picasso, vinci"

    outputs = stable_diffusion_generater_img2img(prompt=prompt, negative_prompt="", init_images=[init_img_base64], styles=[""])
    for idx, image in enumerate(outputs):
        image.save(f"./image_{idx}.jpg")
    pass

# def main():
#     template = json.load(open("template_prompt.json"))

#     key_prompt = "sks"
#     class_prompt = "woman"

#     for opt in tqdm(template):
#         fol_id = opt["id"]
#         if not os.path.exists(f"{SAVE_DIR}/{fol_id}"):
#             os.makedirs(f"{SAVE_DIR}/{fol_id}")

#         prompt = opt["prompt"]
#         prompt = prompt.replace("[V]", key_prompt)
#         prompt = prompt.replace("[S]", class_prompt)

#         cnt = 0
#         for _ in range(5):
#             images = stable_diffusion_generater_txt2img(prompt = prompt, 
#                                                 negative_prompt = opt["negative_prompt"],
#                                                 styles = opt["style"].split(","))
#             for idx, image in enumerate(images):
#                 image.save(f"{SAVE_DIR}/{fol_id}/image_{idx+cnt}.jpg")
#             cnt += len(images)

        # break

if __name__=="__main__":
    main()