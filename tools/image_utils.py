import io
import base64
import codecs
import numpy as np
from typing import Union

from PIL import Image


def read_image(path: str) -> np.ndarray:
    image = Image.open(path).convert("RGB")

    # numpy array format (H, W, C=3), channel order RGB
    return np.asarray(image)


def image_to_base64(image0: np.ndarray) -> str:
    image = Image.fromarray(image0, "RGB")

    buffered = io.BytesIO()
    image.save(buffered, format="PNG")

    return base64.b64encode(buffered.getvalue()).decode()


def base64_to_bytearr(byte64_str: str) -> bytes:
    bytearr = codecs.decode(codecs.encode(byte64_str, encoding="ascii"), encoding="base64")
    return bytearr


def base64_to_image(byte64_str: str, grayscale: bool = False):
    bytearr = base64_to_bytearr(byte64_str)

    if grayscale:
        return Image.open(io.BytesIO(bytearr)).convert("L")

    return Image.open(io.BytesIO(bytearr)).convert("RGB")


def resize(image0: np.ndarray,
           new_shape: Union[int, tuple[int, ...]] = (360, 360)) -> np.ndarray:

    # image current shape (height, width)
    old_shape = image0.shape[:2]
    if isinstance(new_shape, int):
        new_shape = (new_shape, new_shape)

    # compute padding
    r = min(new_shape[0] / old_shape[0], new_shape[1] / old_shape[1])
    new_unpad = tuple([int(round(x * r)) for x in old_shape])  # (h,w)
    dh, dw = new_shape[0] - new_unpad[0], new_shape[1] - new_unpad[1]  # h,w padding

    image = Image.fromarray(np.uint8(image0))
    if old_shape != new_unpad:
        image = image.resize(new_unpad[::-1], resample=Image.LANCZOS, reducing_gap=2)

    return np.asarray(image)
