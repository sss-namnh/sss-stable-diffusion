#============================
host = "http://192.168.20.156"
port = 3001

txt2img_endpoint = "sdapi/v1/img2img"
img2img_endpoint = ""

URL = f"{host}:{port}/{txt2img_endpoint}"
#============================

SAVE_DIR = "outputs"

#============================
request_body = {
            "enable_hr": False,
            "denoising_strength": 0,
            "firstphase_width": 0,
            "firstphase_height": 0,
            "prompt": "",
            "styles": [""],
            "seed": -1,
            "subseed": -1,
            "subseed_strength": 0,
            "seed_resize_from_h": -1,
            "seed_resize_from_w": -1,
            "batch_size": 1,
            "n_iter": 1,
            "steps": 50,
            "cfg_scale": 7,
            "width": 512,
            "height": 512,
            "restore_faces": False,
            "tiling": False,
            "negative_prompt": "",
            "eta": 0,
            "s_churn": 0,
            "s_tmax": 0,
            "s_tmin": 0,
            "s_noise": 1,
            "override_settings": {},
            "sampler_index": "DPM2 Karras"
            }

# =================================
request_body_img2img = {
    "init_images": [
        ""
    ],
    "resize_mode": 0,
    "denoising_strength": 0.35,
    "mask": "",
    "mask_blur": 4,
    "inpainting_fill": 0,
    "inpaint_full_res": True,
    "inpaint_full_res_padding": 0,
    "inpainting_mask_invert": 0,
    "prompt": "",
    "styles": [""],
    "seed": -1,
    "subseed": -1,
    "subseed_strength": 0,
    "seed_resize_from_h": -1,
    "seed_resize_from_w": -1,
    "batch_size": 4,
    "n_iter": 1,
    "steps": 55,
    "cfg_scale": 30,
    "width": 512,
    "height": 512,
    "restore_faces": False,
    "tiling": False,
    "negative_prompt": "",
    "eta": 0,
    "s_churn": 0,
    "s_tmax": 0,
    "s_tmin": 0,
    "s_noise": 1,
    "override_settings": {},
    "sampler_index": "DDIM",
    "include_init_images": True
    }