import gradio as gr
import cv2 
from api_request import stable_diffusion_generater_img2img
import json
from tools.image_utils import image_to_base64 


#============================
STYLE_MAP = json.load(open("/home/namnh/CS410/API/city_and_scence_template.json"))
STYLE_LIST = [v["topic_name"] for v in STYLE_MAP]

# CONFIG


#============================
def stable_diffusion(init_images,topic_name, seed,step, batch_size):
    batch_size = 4

    base64_image = image_to_base64(init_images)
    for k in STYLE_MAP:
        if k["topic_name"] == topic_name:
            prompt_input = k["prompt"]
            images = stable_diffusion_generater_img2img(prompt=prompt_input, negative_prompt="",init_images=[base64_image], styles=[""], steps=step,seed=seed, batch_size=batch_size)
            log = f'''- Prompt: "{prompt_input}"\n- Topic name: "{topic_name}"\n- Random seed: {seed}'''
            images.append(log)
            return images


with gr.Blocks() as demo:

    gr.Markdown("## Stable Diffusion AI - image2image Art Style")
    gr.Markdown("**Model: stable-diffusion-v1.5**")

    with gr.Row() as row_root:

        with gr.Column() as row_input:

            gr.Markdown("**Upload the image you want to use in here**")
            image_init = gr.Image(label="Image Init")

            style = gr.Dropdown(STYLE_LIST, value=STYLE_LIST[0], label="Style Input")

            with gr.Row() as row_:
                seed = gr.Textbox(-1, label="Seed Random") 
                step = gr.Slider(40,100,50,step=1, label='Step')
                batch_size = gr.Slider(minimum = 1, maximum = 4,value=4, step=1,label="Batch Size") 

            log = gr.Textbox(label="Log Output")
            greet_btn = gr.Button("Generate", variant='primary')

        with gr.Column() as row_output2:

            with gr.Row() as row_output1:
                output1 = gr.Image(label="Outputs")
                output2 = gr.Image(label="Outputs")

            with gr.Row() as row_output2:
                output3 = gr.Image(label="Outputs")
                output4 = gr.Image(label="Outputs")


    greet_btn.click(fn=stable_diffusion, inputs=[image_init,style, seed,step, batch_size], 
                    outputs=[output1, output2, output3, output4, log])
                                       
demo.launch(share=True)
 